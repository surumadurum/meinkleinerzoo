﻿using System;
using System.Collections.Generic;

namespace Zoo
{
    public abstract class Kaefig
    {

        public List<Tier> eingesperrteTiere { get; } = new List<Tier>();

        public int kapazitaet { get; }
        public int belegung 
        { 
            get
            {
                return eingesperrteTiere.Count;
            }
        }
        public int freiePlaetze
        {
            get
            {
                return kapazitaet - belegung;
            }
        }

        public Kaefig(int kapazitaet)
        {
            this.kapazitaet = kapazitaet;
        }

        public void NeuesTierEinsperren(Tier tier)
        {
            if (freiePlaetze > 0)
                eingesperrteTiere.Add(tier);
            else
                throw new Exception("[Kaefig] Kaefig schon voll.");
        }
    }



    public class RaubtierKaefig : Kaefig
    {
        public RaubtierKaefig(int kapazitaet) : base(kapazitaet) { }
    }

    public class Freigehege : Kaefig
    {
        public Freigehege(int kapazitaet) : base(kapazitaet) { }
    }

}
