﻿using System;
using System.IO;

namespace Zoo
{
    class Program
    {
        public static void Main(string[] args)
        {
            Zoomanager.Instance.standartKapazitaet = 2;

            while (true)
            {
                MenuHaupt();
            }
        }

        private static void MenuHaupt()
        {
            Console.Clear();
            Console.WriteLine("(A) - neues Tier");
            Console.WriteLine("(B) - Bestandsliste");
            Console.WriteLine("(Z) - Ende");

            var key = Console.ReadKey();
            Console.WriteLine();  //CRLF

            switch (key.KeyChar)
            {
                case 'a':
                case 'A':
                    MenuNeuesTier();
                    break;

                case 'b':
                case 'B':
                    GebeAusInventur();
                    break;

                case 'z':
                case 'Z':
                    Environment.Exit(0);
                    break;

            }
        }

        private static void MenuNeuesTier()
        {
            Console.WriteLine("1 - Loewe");
            Console.WriteLine("2 - Antilope");
            Console.WriteLine("3 - Gazelle");
            Console.WriteLine("0 - Zurueck");

            var sKey = Console.ReadKey();
            Console.WriteLine();

            if (sKey.KeyChar == '0') return;    // zurueck

            Console.WriteLine("Bitte Namen eingeben: ");
            string name = Console.ReadLine();

            Tier tier = null;
            switch (sKey.KeyChar)
            {
                case '1':
                    tier = new Loewe(name);
                    break;
                case '2':
                    tier = new Antilope(name);
                    break;
                case '3':
                    tier = new Gazelle(name);
                    break;
                default:
                    break;
            }
            if (tier != null)
                Zoomanager.Instance.NeuesTierAufnehmen(tier);
        }


        // Gibt den Bestand aus
        public static void GebeAusInventur()
        {

            Zoomanager.Instance.GebeAusInventur(Console.Out);   // Bestand auf Console ausgeben

            Console.WriteLine("Bitte Taste druecken");
            Console.ReadKey();
        }
    }
}
