﻿namespace Zoo
{
    public abstract class Tier
    {
        public string name { get; }

        public Tier(string name)
        {
            this.name = name;
        }
    }

    public abstract class Raubtier : Tier
    {
        public Raubtier(string name) : base(name) { }
    }


    public abstract class GanzFriedlichesTier : Tier
    {
        public GanzFriedlichesTier(string name) : base(name) { }
    }

    public class Loewe : Raubtier
    {
        public Loewe(string name) : base(name) { }
    }

    public class Antilope : GanzFriedlichesTier
    {
        public Antilope(string name) : base(name) { }
    }

    public class Gazelle : GanzFriedlichesTier
    {
        public Gazelle(string name) : base(name) { }
    }
}