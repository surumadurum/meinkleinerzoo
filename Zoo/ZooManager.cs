﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Zoo
{

    class Zoomanager
    {
        // Machen wir ein Singleton draus, es gibt nur einen Zoomanager
        // --- start singleton
        private static Zoomanager instance = null;

        public static Zoomanager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Zoomanager();
                }
                return instance;
            }

        }

        private Zoomanager()
        {

        }

        // --- end singleton ---


        /*Anm.: Hier bin ich mir unsicher, ob die "Datenschachtelung" Kaefig->[Tiere im Kaefig] 
        * (in jedem Kaefig-Objekt ist eine Liste mit den darin gehaltenen Tieren) gut ist,
        * oder ob es besser waere, eine Liste mit Kaefigen und eine Liste mit Tieren vorzuhalten, die ueber ein Mapping       
        * miteinander verbunden sind. Zweitere Variante liese sich wahrscheinlich besser/ueberschaulicher in einer SQL DB abbilden
        */
        //TODO: Speichermanagement entkoppelt und in separate Klasse auslagern, um verschiedene Datenquellen einfacher anbinden zu koennen (SQL)
        public List<Kaefig> kaefige { get; } = new List<Kaefig>();

        public int standartKapazitaet { get; set; } = 2;

        // Neues Tier katalogisieren und in den Kaefig bringen
        public void NeuesTierAufnehmen(Tier tier)
        {
            Kaefig kaefig = null;

            if (tier is Raubtier)
                kaefig = GetFreierKaefig<RaubtierKaefig>();

            else if (tier is GanzFriedlichesTier)
                kaefig = GetFreierKaefig<Freigehege>();

            if (kaefig != null)
                kaefig.NeuesTierEinsperren(tier);
            else
                throw new Exception("[Zoomanager] Kein passender Kaefig fuer Tier +" + tier.name + "] gefunden.");

        }

        // Gibt den aktuell noch nicht voll belegten Kaefig zurueck bzw. erstellt evtl. einen neuen
        private Kaefig GetFreierKaefig<T>()
        {
            // Durchsuche alle Kaefige
            foreach (Kaefig kaefig in kaefige)
            {
                // Ist der aktuelle Kaefig passend fuer die Tiergattung
                // Und ist noch ein Platz frei
                if (kaefig is T && kaefig.freiePlaetze > 0)
                {
                    return kaefig;
                }
            }

            //Kein freier Platz gefunden, neuen Kaefig bauen
            //TODO: evtl. Abfrage: Soll der Kaefig groesser sein als standartKapazitaet?
            Kaefig neuerKaefig = BaueNeuenKaefig<T>();

            return neuerKaefig;

        }

        public Kaefig BaueNeuenKaefig<T>()
        {
            Kaefig neuerKaefig = null;
            if (typeof(T) == typeof(RaubtierKaefig))
                neuerKaefig = new RaubtierKaefig(standartKapazitaet);

            else if (typeof(T) == typeof(Freigehege))
                neuerKaefig = new Freigehege(standartKapazitaet);

            if (neuerKaefig != null)
                kaefige.Add(neuerKaefig);

            return neuerKaefig;
        }

        // Schreibt den Bestand in den uebergebenen Stream
        public void GebeAusInventur(TextWriter stream)
        {
            for (int i = 0; i < kaefige.Count; i++)
            {
                var kaefig = kaefige[i];

                string kaefigTyp = (kaefig is RaubtierKaefig) ? "Raubtierkaefig" : "Freigehege";

                foreach (var tier in kaefig.eingesperrteTiere)
                {
                    stream.WriteLine("Kaefig {0}[{1}]: {2}", i + 1, kaefigTyp, tier.name);
                }
            }

        }
    }
}